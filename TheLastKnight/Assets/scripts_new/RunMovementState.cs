﻿using System;
using UnityEngine;
namespace LastKnightCore
{
    [Serializable]
    public class RunMovementState:IMovementState
    {
        public void OnStateEnter()
        {
            
        }

        public void OnStateExit()
        {
           
        }

        public float MovementRatio;
        public float GetMovementRatio()
        {
            return MovementRatio;
        }
        CharacterMovementController _movController;

        public void Init(CharacterMovementController movController)
        {
            _movController = movController;
        }

        public virtual void ProcessState(CharacterInput input)
        {
          
            _movController.Rotate(input.Direction);
            if (input.MoveInput != 0)
            {
                _movController.Accelerate(MovementRatio);
            }
            else
            {
                _movController.Decelerate(MovementRatio);
            }

            if (input.Hit.GetIsActive())
            {
                _movController.SetState(MovementStateList.Attack);
            }
            if (input.StrongHit.GetIsActive())
            {
                _movController.SetState(MovementStateList.StrongAttack);
            }
            if (_movController.IsGrounded&&input.Jump.GetIsActive())
            {
                _movController.SetState(MovementStateList.Jump);
            }
            if (!_movController.IsGrounded)
                _movController.SetState(MovementStateList.Air);
        }
    }
}

