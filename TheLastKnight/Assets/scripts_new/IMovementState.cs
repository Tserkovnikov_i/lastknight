﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LastKnightCore
{
    public interface IMovementState
    {
     

      
        float GetMovementRatio();
         void Init(CharacterMovementController movController);

         void ProcessState(CharacterInput input);
        void OnStateEnter();
        void OnStateExit();
    }
}