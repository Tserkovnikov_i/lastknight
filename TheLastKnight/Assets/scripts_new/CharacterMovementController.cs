﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace LastKnightCore
{
    public enum MovementStateList
    {
        Run,
        Air,
        Jump,
        Land,
        Attack,
        StrongAttack
    }
    [Serializable]
    public class CharacterMovementController : MonoBehaviour
    {
    
        public float RunSpeed;
        public float JumpSpeed;
        public float Acceleration;
        public float RotSpeed = 160;
        public BaseCharacterController input;
        public RunMovementState RunState;
        public JumpMovementState JumpState;
        public AirMovementState AirState;
        public AttackMovementState AttackState;
        public LandMovementState LandState;
        public StrongAttackMovementState StrongAttackState;
        AnimationManager _aManager;
        AttackAnimEvents _aEvents;
        GroundCheckScript _groundCheck;
        bool _isGrounded;

        public bool IsGrounded
        {
            get
            {
                return _isGrounded;
            }
            set
            {
                _isGrounded = value;
            }
        }

        Weapon _weapon;

        Vector3 _direction;
        Vector3 _currentDirection;
        float _speed;
        public AttackAnimEvents AEvents
        {
            get
            {
                return _aEvents;
            }
            set
            {
                _aEvents = value;
            }
        }

        public AnimationManager AManager
        {
            get
            {
                return _aManager;
            }
            set
            {
                _aManager = value;
            }
        }

        GroundCheckScript _gCheck;
        RigidBodyMover _mover;

        public IMovementState _currentState;

        public void SetState(MovementStateList newState)
        {
            
            _currentState.OnStateExit();
            switch (newState)
            {
                case MovementStateList.Run:
                    _currentState = RunState;
                    break;
                case MovementStateList.Jump:
                    _currentState = JumpState;
                    break;
                case MovementStateList.Air:
                    _currentState = AirState;
                    break;
                case MovementStateList.Land:
                    _currentState = LandState;
                    break;
                case MovementStateList.Attack:
                    _currentState = AttackState;
                        break;
                case MovementStateList.StrongAttack:
                    _currentState = StrongAttackState;
                    break;
                default:
                    break;
            }
            _currentState.OnStateEnter();
        }


        void Start()
        {
            _direction = Vector3.forward;
            _currentDirection = Vector3.forward;
            _weapon = GetComponentInChildren<Weapon>();
            _aEvents = GetComponent<AttackAnimEvents>();
            _groundCheck = GetComponentInChildren<GroundCheckScript>();
            _isGrounded = true;
            RunState.Init(this);
            JumpState.Init(this);
            AirState.Init(this);
            LandState.Init(this);
            AttackState.Init(this);
            StrongAttackState.Init(this);
            _aManager = new AnimationManager();
            _aManager.Init(GetComponent<Animator>());
            _currentState = RunState;
            _mover = new RigidBodyMover(GetComponent<Rigidbody>(), JumpSpeed);
        }

        // Use this for initialization
        void Init(GameObject character,Rigidbody rigidBody, AnimationManager aManager)
        {
            _currentState = RunState;
            _aManager = aManager;
            _gCheck = character.GetComponentInChildren<GroundCheckScript>();
            _mover = new RigidBodyMover(rigidBody,JumpSpeed);

        }
	
        // Update is called once per frame
        void Update()
        {
            _aManager.Process();
            if(_groundCheck!=null)
                _isGrounded = _groundCheck.IsGrounded;
            
            float angle1 = Vector3.Angle(Vector3.forward, _currentDirection);
            float angle2= Vector3.Angle(Vector3.forward, _direction);


            _currentDirection = Vector3.Slerp(_currentDirection, _direction, Time.deltaTime*5);

            _currentState.ProcessState(input.CInput);
            _aManager.SetSpeed(_speed);
            _aManager.SetYSpeed(_mover.GetVelocity().y);

            _mover.Move(_currentDirection,_speed);
            _weapon.IsInActivePhase = _aEvents.InDamageZone;
        }

        public void Rotate(Vector3 direction)
        {
            _direction = direction;
        }
        public void Accelerate(float moveRatio)
        {

            if (Mathf.Abs(_speed) < RunSpeed*moveRatio-1)
            {
                _speed += Acceleration* Time.deltaTime ;
            }
            else
            {
                _speed = RunSpeed*moveRatio ;
            }
        }
        public void Decelerate(float moveRatio)
        {
            if (_speed > 0)
            {
                _speed -= Acceleration * Time.deltaTime;
                if (_speed < 0)
                    _speed = 0;
            }
            if (_speed < 0)
            {
                _speed += Acceleration * Time.deltaTime;
                if (_speed > 0)
                    _speed = 0;
            }
        }
        public void StopMovement()
        {
            _speed = 0;
        }
        public void Jump()
        {
            _mover.Jump();
        }
    }
}