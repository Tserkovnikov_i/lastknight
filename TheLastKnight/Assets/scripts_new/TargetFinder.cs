﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LastKnightCore
{
public class TargetFinder : MonoBehaviour {

    public float Angle=35;
    public float Distance = 50;
    public int Raycount = 6;
        public CameraScript PCamera;
        private Transform _target;

   

    public Transform Target
    {
        get
        {
            return _target;
        }
        private set
        {
            _target = value;
        }
    }

    // Use this for initialization
    void Start () {
        
    }

    Transform CastRay(Vector3 dir)
    {
            Transform res = null;
        RaycastHit hit = new RaycastHit();
            int mask1 = 1 << 12;
            int mask2 = 1 << 16;

            int mask = ((mask1 | mask2));
            Vector3 pos = gameObject.transform.position+new Vector3(0,.6f,0);

        if (Physics.Raycast (pos, dir, out hit, Distance,mask))
        {
           
                res = hit.transform;
                Debug.DrawLine(pos, hit.point, Color.green);
           
        }
        else
        {
            Debug.DrawRay(pos, dir *Distance, Color.red);
        }
        return res;
    }

    Transform Scan()
    {
        float currentAngle = -Angle / 2;
        float step = Angle / Raycount;
        Transform res = null;
        for (int i = 0; i < Raycount; i++)
        {
                Vector3 dir = Vector3.zero;

                if (PCamera == null)
                    dir = Quaternion.AngleAxis(currentAngle, Vector3.up) * gameObject.transform.forward;
                else
                {
                    dir = PCamera.Direction;
                    dir.y = 0;
                    dir = Quaternion.AngleAxis(currentAngle, Vector3.up) * dir;
                }


                res = CastRay(dir);
                if (res != null) return res;

            currentAngle += step;
        }
        return res;
    }

    // Update is called once per frame
    void Update () {
            _target = Scan();
    }
}
}