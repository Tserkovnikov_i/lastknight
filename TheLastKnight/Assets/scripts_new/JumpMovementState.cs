﻿using System;
using UnityEngine;
namespace LastKnightCore
{
    [Serializable]
    public class JumpMovementState:IMovementState
    {
        bool _jStarted;
        public void OnStateEnter()
        {
            _movController.AManager.SetInJStartState(true);
            _jStarted = false;
        }

        public void OnStateExit()
        {
            
        }

        public float MovementRatio = 0.0f;
        public float GetMovementRatio()
        {
            return MovementRatio;
        }
        CharacterMovementController _movController;

        public void Init(CharacterMovementController movController)
        {
            _movController = movController;
            _jStarted = false;
        }

        public virtual void ProcessState(CharacterInput input)
        {
            if (_movController.AManager.AttackStateTime() >= .8f&&!_jStarted)
            {
                
                _movController.Jump();
                _jStarted = true;
            }
            if (!_movController.AManager.GetInJStartState())
            {
               
                _movController.SetState(MovementStateList.Air);
            }
        }
    }
}