﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LastKnightCore
{
    public class AnimMHelper : MonoBehaviour
    {

        [SerializeField]
        AnimationManager _aManager;
        [Range(0,1)]
        public float Speed = 0;
        public Attacks CAttack = Attacks.StandartAttackRight;
        Rigidbody _rigidBody;
        GameObject _hero;
        // Use this for initialization
        void Start()
        {
            _aManager = new AnimationManager();
            _aManager.Init(GetComponentInChildren<Animator>());
            _rigidBody = GetComponent<Rigidbody>();
            _hero = GameObject.Find("Hero");
        }
	
        // Update is called once per frame
        void Update()
        {
            _aManager.Process();
            _aManager.SetSpeed(Speed);
            if (_aManager.MoveByRootMotion)
            {
                _rigidBody.transform.position = _hero.transform.position;
            }
        }
        public void PlayAttack()
        {
            _aManager.PlayAttack(CAttack);
        }
    }
}