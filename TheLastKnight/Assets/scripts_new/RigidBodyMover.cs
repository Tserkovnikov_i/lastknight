﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace LastKnightCore
{
    public class RigidBodyMover
    {
        Rigidbody _rigidBody;
        float _jumpSpeed;
        bool _jump;
        public RigidBodyMover( Rigidbody rigidBody, float jumpSpeed)
        {
            _rigidBody = rigidBody;
            _jumpSpeed = jumpSpeed;
            _jump = false;

        }
        public void Jump()
        {
            _jump = true;
        }
        public void Move(Vector3 direction, float speed)
        {
            if (_jump)
            {
                _rigidBody.AddForce(new Vector3(0,1,0)*_jumpSpeed,ForceMode.VelocityChange);
                _jump = false;
            }
            Vector3 velocity = direction * speed;
            _rigidBody.MoveRotation( Quaternion.LookRotation(direction));
            _rigidBody.MovePosition(_rigidBody.transform.position + velocity * Time.deltaTime);
        }
        public Vector3 GetVelocity()
        {
            return _rigidBody.velocity;
        }
    }
}

