﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackAnimEvents : MonoBehaviour {

     bool _inDamageZone;

    public bool InDamageZone
    {
        get
        {
            return _inDamageZone;
        }
        private set
        {
            _inDamageZone = value;
        }
    }

    public bool _inComboPassZone;

    public bool InComboPassZone
    {
        get
        {
            return _inComboPassZone;
        }
        private set
        {
            _inComboPassZone = value;
        }
    }

	// Use this for initialization
	void Start () {
        _inDamageZone = false;
        _inComboPassZone = false;
	}
	
    void StartDamage()
    {
        Debug.Log("Damage!");
        _inDamageZone = true;
    }
    void EndDamage()
    {
        _inDamageZone = false;
    }
    void StartComboPass()
    {
        _inComboPassZone = true;
    }
    void EndComboPass()
    {
        _inComboPassZone = false;
    }
	
}
