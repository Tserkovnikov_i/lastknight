﻿using System;
using UnityEngine;
namespace LastKnightCore
{
    [Serializable]
    public class AttackMovementState:IMovementState
    {
        int _comboCounter;
        bool _comboActive;
        public float GetMovementRatio()
        {
            return 0;
        }

        CharacterMovementController _movController;
        public void OnStateEnter()
        {
            _movController.StopMovement();
            _movController.AManager.PlayAttack(Attacks.StandartAttackRight);

        }

        public void OnStateExit()
        {
            _comboCounter = 0;
        }

        public void Init(CharacterMovementController movController)
        {
            _movController = movController;
            _comboCounter = 0;
            _comboActive = false;
        }

        public  void ProcessState(CharacterInput input)
        {
            _movController.Rotate(input.Direction);
            if (input.Hit.GetIsActive() && _movController.AEvents.InComboPassZone&&!_comboActive)
            {
                _comboActive = true;
                Debug.Log("combo"+_comboCounter);
            }
            if (_movController.AManager.AttackStateTime() >= .8f&&_movController.IsGrounded)
            {
                if(_comboActive)
                {
                    _comboActive = false;
                    _comboCounter++;
                    if(_comboCounter%2==0)
                    _movController.AManager.PlayAttack(Attacks.StandartAttackRight);
                    else
                        _movController.AManager.PlayAttack(Attacks.StandartAttackLeft);
                }
            }
            if (_movController.AManager.AttackStateTime() >= 1)
            {
                if (!_comboActive)
                    _movController.SetState(MovementStateList.Run);
                
            }

        }

    }
}