﻿using System;
using UnityEngine;
namespace LastKnightCore
{
    [Serializable]
    public class AirMovementState:IMovementState
    {
        public void OnStateEnter()
        {
            _movController.AManager.SetInAir(true);
        }

        public void OnStateExit()
        {
            _movController.AManager.SetInAir(false);
        }

        public float MovementRatio = 0.5f;
        public float GetMovementRatio()
        {
            return MovementRatio;
        }
        CharacterMovementController _movController;

        public void Init(CharacterMovementController movController)
        {
            _movController = movController;
        }

        public virtual void ProcessState(CharacterInput input)
        {
            _movController.Rotate(input.Direction);
            if (input.MoveInput != 0)
            {
                _movController.Accelerate(MovementRatio);
            }
            else
            {
                _movController.Decelerate(MovementRatio);
            }
            if (_movController.IsGrounded)
                _movController.SetState(MovementStateList.Land);

        }
    }
}

