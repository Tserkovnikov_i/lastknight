﻿using System;
using UnityEngine;

namespace LastKnightCore
{
    [Serializable]
    public class StrongAttackMovementState:IMovementState
    {
        public float GetMovementRatio()
        {
            return 0;
        }

        CharacterMovementController _movController;
        public void OnStateEnter()
        {
            _movController.StopMovement();
            _movController.AManager.PlayAttack(Attacks.StrongAttack);

        }

        public void OnStateExit()
        {
        }

        public void Init(CharacterMovementController movController)
        {
            _movController = movController;
        }

        public  void ProcessState(CharacterInput input)
        {
           
            if (_movController.AManager.AttackStateTime() >= 1)
            {
              
                    _movController.SetState(MovementStateList.Run);

            }

        }

    }
}

