﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace LastKnightCore
{
    public enum Attacks
    {
        StandartAttackRight,
        StandartAttackLeft,
        StrongAttack
    }
    [Serializable]   
    public class AnimationManager 
    {

        Animator _animator;
        bool _moveByRootMotion;
       
        string[] _attacks =
            {
                "Attack1",
                "Attack2",
                "AttackStrong"
            };
      
        public bool MoveByRootMotion
        {
            get {
                return _animator.applyRootMotion;
            }
            set
            {
                _animator.applyRootMotion = value;

            }
        }
        public void SetInAir(bool inAir)
        {
            _animator.SetBool("InAir",inAir);
        }
        public bool GetInAir()
        {
            return _animator.GetBool("InAir");
        }
        public void SetInLandState(bool inLS)
        {
            _animator.SetBool("InLandState",inLS);
        }
        public bool GetInLandState()
        {
            return _animator.GetBool("InLandState");
        }
        public void SetInJStartState(bool inJS)
        {
            _animator.SetBool("InJStartState",inJS);
        }
        public bool GetInJStartState()
        {
            return _animator.GetBool("InJStartState");
        }
        public void Init(Animator animator)
        {
            _animator = animator;
            _moveByRootMotion = false;

        }
        // Use this for initialization
        public void SetSpeed(float speed)
        {
            _animator.SetFloat("Speed", speed);
        }
        public void SetYSpeed(float speed)
        {
            _animator.SetFloat("ySpeed", speed);
        }

        // Update is called once per frame
        void PlayAttack(int attackId)
        {
            _animator.CrossFade(_attacks[attackId],.01f);

        }
        public void PlayAttack(Attacks attack)
        {
            PlayAttack((int)attack);
        }

        public void ContinueAttack()
        {
            _animator.SetTrigger("ComboNext");
        }
        public void Process()
        {
            _moveByRootMotion = _animator.GetBool("movedByRootMotion");
            _animator.applyRootMotion = _moveByRootMotion;

        }
        public float AttackStateTime()
        {
            AnimatorStateInfo stateInfo = _animator.GetCurrentAnimatorStateInfo(2);
            return stateInfo.normalizedTime;
        }
    }
}