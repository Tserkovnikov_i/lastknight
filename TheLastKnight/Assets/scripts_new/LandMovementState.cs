﻿using System;
using UnityEngine;
namespace LastKnightCore
{
    [Serializable]
    public class LandMovementState:IMovementState
    {
        public void OnStateEnter()
        {
            _movController.AManager.SetInAir(false);
        }

        public void OnStateExit()
        {
           
        }

        public float MovementRatio = 0.0f;
        public float GetMovementRatio()
        {
            return MovementRatio;
        }
        CharacterMovementController _movController;

        public void Init(CharacterMovementController movController)
        {
            _movController = movController;
        }

        public virtual void ProcessState(CharacterInput input)
        {
            if (!_movController.AManager.GetInLandState())
                _movController.SetState(MovementStateList.Run);
        }
    }
}