﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LastKnightCore
{
public class ObjectPool : MonoBehaviour {

        public int PoolSize = 10;
        public GameObject SourceObject;
        private IPoolObject _sourcePoolObject;
        private List<IPoolObject> _poolObjects;
        private int _objectsInPool;

	// Use this for initialization
	void Awake () {
            _sourcePoolObject = SourceObject.GetComponent<IPoolObject>();
           
                _poolObjects = new List<IPoolObject>(PoolSize);
                for (int i = 0; i < PoolSize; i++)
                {
                    _poolObjects.Add(_sourcePoolObject.Instantiate());
                    _poolObjects[i].IsActive = false;
                }
                _objectsInPool = PoolSize;


	}
    public void Spawn(Vector3 position)
    {
            if (_objectsInPool != 0)
            {
                for (int i = 0; i < PoolSize; i++)
                {
                    if (!_poolObjects[i].IsActive)
                    {
                        _poolObjects[i].Spawn(position);
                        _poolObjects[i].IsActive = true;
                        _objectsInPool--;
                        return;
                    }
                }
            }
            else
            {
                _poolObjects.Add(_sourcePoolObject.Instantiate());
                PoolSize++;
                _poolObjects[PoolSize - 1].Spawn(position);
                _poolObjects[PoolSize-1].IsActive = true;
                Debug.Log("Pool size increased!");
            }
    }
	
	
}
}