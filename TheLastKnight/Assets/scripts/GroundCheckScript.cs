﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundCheckScript : MonoBehaviour {
    public bool IsGrounded { get; private set;}
    void OnTriggerExit(Collider collider)
    {
        IsGrounded = false;
    }
    void OnTriggerEnter(Collider collider)
    {
        IsGrounded = true;
    }
    void OnTriggerStay(Collider collider)
    {
        
        IsGrounded = true;
    }
}
