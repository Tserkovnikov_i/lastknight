﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
namespace LastKnightCore
{
    public delegate void ActionEventDel(bool jump);

    [Serializable]
    public class RunMovement:BaseMovement
    {
       
        bool _jump;
        bool _hit;
        bool _allowJump;

        public bool AllowJump
        {
            get
            {
                return _allowJump;
            }
            set
            {
                _allowJump = value;
            }
        }

        public event ActionEventDel OnJump; 
        public event ActionEventDel OnAttack; 
        [SerializeField] float JumpImpulseSpeed= 10.0f;
        public override void Init(CharacterMovement cMovement)
        {
            base.Init(cMovement);
            _allowJump = true;
            _jump = false;
        }
        public override void CheckInput(CharacterInput input)
        {
            base.CheckInput(input);

            if (input.Jump.GetIsActive()&&_allowJump)
            {
                _jump = true;
                OnJump.Invoke(true);
                _allowJump = false;
            }
            if (input.Hit.GetIsActive())
            {
                _hit = true;
                OnAttack.Invoke(true);
            }
           
        }

        public override void ProcessMove()
        {
            base.ProcessMove();
            if (_jump)
            {
                _jump = false;
                _rigidBody.AddForce(_characterMovement.Velocity+ new Vector3(0,JumpImpulseSpeed,0),ForceMode.VelocityChange);
            }
            if (_hit)
            {
                _hit = false;
                _characterMovement.State = CharacterMovementState.Attack;
                 
            }
        }

    }
}

