﻿using System;
using UnityEngine;
using UnityEngine.Animations;
namespace LastKnightCore
{
    
    public class CharacterAnimationController
    {
        
        private Animator _animator;
        public CharacterAnimationController(Animator animator)
        {
            _animator = animator;
        }

        void SetRunSpeed(float speed)
        {
            _animator.SetFloat("speed",speed);
        }
        void SetJumpSpeed(float speed)
        {
            _animator.SetFloat("ySpeed", speed);
        }

        void StartJumping()
        {
            _animator.SetTrigger("jump");
        }

    }
}

