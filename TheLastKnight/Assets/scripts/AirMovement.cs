﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
namespace LastKnightCore
{
    [Serializable]
    public class AirMovement:BaseMovement
    {
     
        public override void Init(CharacterMovement characterMovement)
        {
            base.Init(characterMovement);
        }
        public override void CheckInput(CharacterInput input)
        {
            base.CheckInput(input);
        }

        public override void ProcessMove()
        {
            base.ProcessMove();

        }

    }
}

