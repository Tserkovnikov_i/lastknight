﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace LastKnightCore
{
    public delegate void DamageCharacterEvent(Character hited);
public class Weapon : MonoBehaviour {

        public event DamageCharacterEvent onDamageCharacter;
    public float Damage = 10.0f;
        public bool IsInActivePhase { get; set;}

        private ObjectPool _hitParticlesPool;
        void Awake()
        {
            IsInActivePhase = false;
            _hitParticlesPool = GetComponent<ObjectPool>();
           

        }
	// Use this for initialization
        void OnCollisionExit(Collision collision)
    {
        
    }
        void OnCollisionEnter(Collision collision)
    {
            if (IsInActivePhase&&collision.collider.tag =="Hurtbox")
            {
                onDamageCharacter.Invoke(collision.collider.gameObject.GetComponent<HurtboxScript>().character);
                //_hitParticlesPool.Spawn(collider.gameObject.transform.position);
               
                _hitParticlesPool.Spawn(collision.contacts[0].point);
               
            }
    }
}

}