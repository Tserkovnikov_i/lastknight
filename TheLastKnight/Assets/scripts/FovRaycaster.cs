﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FovRaycaster : MonoBehaviour {

    public float Angle=35;
    public float Distance = 50;
    public int Raycount = 6;
    public string TagToFind = "Player";

    private bool _targetDetected = false;

    public bool TargetDetected
    {
        get
        {
            return _targetDetected;
        }
        private set
        {
            _targetDetected = value;
        }
    }

	// Use this for initialization
	void Start () {
		
	}
	
    bool CastRay(Vector3 dir)
    {
        bool res = false;
        RaycastHit hit = new RaycastHit();
        Vector3 pos = gameObject.transform.position+new Vector3(0,.6f,0);
        if (Physics.Raycast (pos, dir, out hit, Distance))
        {
            if(hit.collider.tag == TagToFind)
            {
                res = true;
                Debug.DrawLine(pos, hit.point, Color.green);
            }
            else
            {
                Debug.DrawLine(pos, hit.point, Color.blue);
            }
        }
        else
        {
            Debug.DrawRay(pos, dir *Distance, Color.red);
        }
        return res;
    }

    bool Scan()
    {
        float currentAngle = -Angle / 2;
        float step = Angle / Raycount;
        bool res = false;
        for (int i = 0; i < Raycount; i++)
        {
            Vector3 dir = Quaternion.AngleAxis(currentAngle, Vector3.up) * gameObject.transform.forward;
            if (CastRay(dir))
                res = true;

            currentAngle += step;
        }
        return res;
    }

	// Update is called once per frame
	void Update () {
        if (Scan())
            _targetDetected = true;
        else
            _targetDetected = false;
	}
}
