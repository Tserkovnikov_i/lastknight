﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace LastKnightCore
{
public class HealthbarUI : MonoBehaviour {

        private Character _character;
        private Image _image;

	// Use this for initialization
	void Start () {
            _character = GetComponentInParent<Character>();
            _image = GetComponentInChildren<Image>();
	}
	
	// Update is called once per frame
	void Update () {
            _image.fillAmount = _character.HP / 100;
	}
}
}