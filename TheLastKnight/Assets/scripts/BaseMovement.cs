﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System;

namespace LastKnightCore
{
    [Serializable]
    public abstract class BaseMovement
    {
        protected CharacterMovement _characterMovement;
        [SerializeField] float MaxSpeed=4;
        [SerializeField] float RotateSpeed=160;
        [SerializeField] float Acelleration=6;

        protected Rigidbody _rigidBody;
        protected Animator _animator;
        protected GroundCheckScript _groundCheck;
        protected Collider _collider;
        protected Vector3 _velocity;

        protected float _front;

        private Vector3 _directionTo;
        private bool _needMovement;
    
//      
//        public  BaseMovement(Rigidbody rigidBody,Animator animator,GroundCheckScript groundCheck, Collider collider, Vector3 direction)
//        {
//            _rigidBody = rigidBody;
//            _animator = animator;
//            _groundCheck = groundCheck;
//            _collider = collider;
//            _velocity = Vector3.zero;
//           
//        }
//        public BaseMovement(CharacterMovement characterMovement)
//        {
//            _characterMovement = characterMovement;
//            _rigidBody =characterMovement.RigidBody;
//            _animator = characterMovement.Animator;
//            _groundCheck = characterMovement.GroundCheck;
//            _collider = characterMovement.Collider;
//
//        }
        public virtual void Init(CharacterMovement characterMovement)
        {
            _characterMovement = characterMovement;
            _rigidBody =characterMovement.RigidBody;
            _animator = characterMovement.Animator;
            _groundCheck = characterMovement.GroundCheck;
            _collider = characterMovement.Collider;
            _needMovement = false;
        }
        public virtual void CheckInput(CharacterInput input)
        {
            
           
            _needMovement = !input.ControlledByAIAgent;

            _front = input.MoveInput;
            _directionTo = input.Direction;
           
        }
        public virtual void ProcessMove()
        {
            
                if (_front != 0)
                    accelerate();
                else
                    decelerate();
            

            if (_needMovement)
            {
                if (_directionTo != Vector3.zero)
                {
                   
                    
                    _characterMovement.Rotate(_directionTo);
                }
            }


        }

     
        void accelerate()
        {
 
            if (Mathf.Abs(_characterMovement.Speed) < MaxSpeed-1)
                {
                _characterMovement.Speed += Acelleration* Time.deltaTime *Mathf.Sign( _front);
                }
                else
                {
                _characterMovement.Speed = MaxSpeed *Mathf.Sign( _front);
                }
           
        }
        void decelerate()
        {
            if (_characterMovement.Speed > 0)
            {
                _characterMovement.Speed -= Acelleration * Time.deltaTime;
                if (_characterMovement.Speed < 0)
                    _characterMovement.Speed = 0;
            }
            if (_characterMovement.Speed < 0)
            {
                _characterMovement.Speed += Acelleration * Time.deltaTime;
                if (_characterMovement.Speed > 0)
                    _characterMovement.Speed = 0;
            }
        }
    }
}

