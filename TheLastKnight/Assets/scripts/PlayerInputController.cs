﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LastKnightCore
{
    
    public class PlayerInputController:BaseCharacterController  {
	
        public float RotSpeed = 160.0f;
        CameraScript _camera;
        float angle = 0;
        Vector3 _moveDir;
        TargetFinder _tFinder;
        Transform _target;
	// Update is called once per frame
        protected override void Awake()
        {
            base.Awake();
            _camera = GetComponent<CameraScript>();
            _moveDir = new Vector3(0, 0, 1);
            _tFinder = GameObject.Find("Player").GetComponent<TargetFinder>();
            _target = null;
            
        }
	    void Update () {
            float vertical = Input.GetAxis("Vertical");
            float horizontal = Input.GetAxis("Horizontal");
            float strife = Input.GetAxis("Strife");
            float view = Input.GetAxis("LookUp");
            bool targeting = Input.GetButtonDown("Targeting");
          if(targeting&&_tFinder.Target!=null&&_target==null)
            {

                _target = _tFinder.Target;
            }
          if(Input.GetButtonUp("Targeting"))
            {
                _target = null;
            }
            if (horizontal > .1f || horizontal < -.1f)
            {
                angle +=Mathf.Sign(horizontal)*RotSpeed*Time.deltaTime;

                _camera.Direction = Quaternion.AngleAxis(Mathf.Sign(horizontal)*RotSpeed*Time.deltaTime, Vector3.up) * _camera.Direction;
                //_moveDir = _camera.Direction;
            }
            if(_target!=null)
            {
               Vector3 d = _target.position - _tFinder.gameObject.transform.position;
                d.y = 0;
                _camera.Direction = d.normalized;
            }
            if (view > .1f || view < -.1f)
            {
                if(_camera.VerticalAngle<5&&view>0)
                _camera.VerticalAngle += RotSpeed*Time.deltaTime;
                if(_camera.VerticalAngle>-70&&view<0)
                    _camera.VerticalAngle -= RotSpeed*Time.deltaTime;
            }
            if (strife!=0||vertical!=0)
            {
                _moveDir = new Vector3(strife, 0, vertical);
                _cInput.MoveInput = _moveDir.normalized.magnitude;
                _moveDir = Quaternion.FromToRotation(new Vector3(0,0,1),_camera.Direction) * _moveDir;

            }
            else
            {
                // _moveDir = Quaternion.FromToRotation(new Vector3(0,0,1),_camera.Direction) * _moveDir;
                if (_target != null)
                {
                    Vector3 d = _target.position - _tFinder.gameObject.transform.position;
                    d.y = 0;
                    _moveDir = d.normalized;
                }
                _cInput.MoveInput = 0;
            }
            _cInput.Direction = _moveDir.normalized;
            if (Input.GetButtonDown("Jump"))
                _cInput.Jump.Activate();
            if (Input.GetButtonUp("Jump"))
                _cInput.Jump.Reset();

            if (Input.GetButtonDown("Fire1"))
            {
                _cInput.Hit.Activate();
               
            }
            if (Input.GetButtonUp("Fire1"))
            {
                _cInput.Hit.Reset();
            }
            if (Input.GetButtonDown("Fire2"))
            {
                _cInput.StrongHit.Activate();

            }
            if (Input.GetButtonUp("Fire2"))
            {
                _cInput.StrongHit.Reset();
            }

	    }
    }
}