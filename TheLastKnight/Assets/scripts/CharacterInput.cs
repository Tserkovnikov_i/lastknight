﻿using System;
using UnityEngine;
namespace LastKnightCore
{
    public class Trigger
    {
        
        private bool _isActive;
        private bool _canSet;
        public bool GetIsActive()
        {
                bool tmp = _isActive;
                _isActive = false;
                return tmp;
        }
        public bool Activate()
        {
            if (_canSet)
            {
                _isActive = true;
                _canSet = false;
                return true;
            }
            else
                return false;
        }

        public void Reset()
        {
            _isActive = false;
            _canSet = true;
        }

        public Trigger()
        {
            Reset();
        }
    }
    public class CharacterInput
    {
       
        public float MoveInput;
        public Trigger Hit;
        public Trigger StrongHit;
        public Trigger Jump;
        public Vector3 Direction;
        public bool ControlledByAIAgent;
        public CharacterInput()
        {
            MoveInput = 0;
            Hit = new Trigger();
            StrongHit = new Trigger();
            Jump = new Trigger();

            Direction = new Vector3(0, 0, 1);
            ControlledByAIAgent = false;
        }
    }
}

