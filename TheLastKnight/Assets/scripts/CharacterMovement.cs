﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using System;
namespace LastKnightCore
{
    
    [Serializable]
    public class CharacterMovement {


        [SerializeField] RunMovement runMovement;
        [SerializeField]  AirMovement airMovement;
        [SerializeField]  AttackMovement attackMovement;
        private bool _jump;
        private void SetJump(bool jump)
        {
            _jump = jump;
        }
        private bool _hit;
        private void SetHit(bool hit)
        {
            _hit= hit;
        }
        private bool _comboHit;
        private void SetComboHit()
        {
            _comboHit = true;
        }
        private bool _land;
        private Vector3 _direction;
        private float _speed;
        private Weapon _weapon;

        public Weapon CWeapon
        {
            get
            {
                return _weapon;
            }
        }

        public float Speed
        {
            get
            {
                return _speed;
            }
            set
            {
                _speed = value;
            }
        }

        private Vector3 _velocity;

        public Vector3 Velocity
        {
            get
            {
                return _velocity;
            }
            set
            {
                _velocity = value;
            }
        }

        protected Rigidbody _rigidBody;

        public Rigidbody RigidBody
        {
            get
            {
                return _rigidBody;
            }

        }

        protected Animator _animator;

        public Animator Animator
        {
            get
            {
                return _animator;
            }
        }

        protected GroundCheckScript _groundCheck;

        public GroundCheckScript GroundCheck
        {
            get
            {
                return _groundCheck;
            }

        }

 
       
        protected Collider _collider;

        public Collider Collider
        {
            get
            {
                return _collider;
            }
        }

        public CharacterMovementState State { get; set;}

        private NavMeshAgent _navAgent;

        public void Init(Rigidbody rigidBody,Animator animator,GroundCheckScript groundCheck, Collider collider, Vector3 direction, Weapon weapon)
        {
            _rigidBody = rigidBody;
            _animator = animator;
            _groundCheck = groundCheck;
            _weapon = weapon;
            _navAgent = rigidBody.gameObject.GetComponent<NavMeshAgent>();
            _collider = collider;
            runMovement.Init(this);
            airMovement.Init(this);
            runMovement.OnJump += SetJump;
            runMovement.OnAttack += SetHit;
            attackMovement.Init(this);
            attackMovement.OnComboHit += SetComboHit;

            _speed = 0;
            _direction = direction;

            State = CharacterMovementState.Grounded;
        }

        public  void CheckInput(CharacterInput input)
        {
            
           // Debug.Log(_direction);
            switch (State)
            {
                case CharacterMovementState.Grounded:
                    runMovement.CheckInput(input);
                    break;
                case CharacterMovementState.InAir:
                    
                    airMovement.CheckInput(input);
                    break;
                case CharacterMovementState.Attack:
                    attackMovement.CheckInput(input);
                    break;
            }

           
        }

        public  void ProcessMove()
        {
            if (_groundCheck.IsGrounded && State == CharacterMovementState.InAir)
            {
                State = CharacterMovementState.Grounded;
                _land = true;
            }
            if (!_groundCheck.IsGrounded && State == CharacterMovementState.Grounded)
            {
                runMovement.AllowJump = true;
                State = CharacterMovementState.InAir;
                _jump = true;
            }
            switch (State)
            {
                case CharacterMovementState.Grounded:
                    runMovement.ProcessMove();
                    break;
                case CharacterMovementState.InAir:
                    airMovement.ProcessMove();
                    break;
                case CharacterMovementState.Attack:
                    attackMovement.ProcessMove();
                    break;
            }

            Move();
        }

        public void ProcessAnimations()
        {
            
            _animator.SetFloat("ySpeed",_rigidBody.velocity.y);
            _animator.SetFloat("speed",_speed);
            if (_jump)
            {
                _jump = false;
                _animator.SetBool("grounded",false);
                _animator.SetBool("jump", true);
            }
            if (_land)
            {
                _land = false;
                _animator.SetBool("grounded",true);
                _animator.SetBool("jump", false);
            }
            if (_hit)
            {
                _hit = false;
                _animator.SetTrigger("Attack1");
            }
            if (_comboHit)
            {
                _comboHit = false;
                _animator.SetTrigger("attackCombo");
            }
        }

         void Move()
        {
            Debug.DrawRay(_rigidBody.transform.position, _direction,Color.blue);
            _velocity = _direction * _speed;
            _velocity.y = _rigidBody.velocity.y;
            _rigidBody.MoveRotation( Quaternion.LookRotation(_direction));
            //_rigidBody.velocity = _velocity;
            if (_navAgent == null)
            {
                _rigidBody.MovePosition(_rigidBody.transform.position + _velocity * Time.deltaTime);
            }
            else
            {
                _navAgent.Move( _velocity * Time.deltaTime);
            }
        }
        public void Rotate(float dir, float rSpeed)
        {
            
            _direction = Quaternion.AngleAxis(dir* rSpeed * Time.deltaTime, new Vector3(0, 1, 0)) * _direction;
        }

        public void Rotate(Vector3 _desiredDirection)
        {
            _direction = _desiredDirection;


        }
    }
}
