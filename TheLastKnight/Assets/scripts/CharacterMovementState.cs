﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace LastKnightCore
{
    public enum CharacterMovementState
    {
        Grounded,
        InAir,
        Attack
    }
}

