﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraScript : MonoBehaviour {
    public GameObject player;

    public float m_smoothTime =.2f;
    public float cameraDistance = 4f;
    public Vector3 offset = new Vector3(0, -10, 0);
    public CollisionHandler collision = new CollisionHandler();
    Vector3 destination = Vector3.zero;Vector3 adjustedDestination = Vector3.zero;Vector3 currVelocity = Vector3.zero;
    Vector3 _direction;

    public Vector3 Direction
    {
        get
        {
            return _direction;
        }
        set
        {
            _direction = value;
        }
    }

    float _verticalAngle;

    public float VerticalAngle
    {
        get
        {
            return _verticalAngle;
        }
        set
        {
            _verticalAngle = value;
        }
    }

    float adjustmentDistance = 0;
	// Use this for initialization
	void Start () {
        collision.Initialize(GetComponentInParent<Camera>());
        collision.UpdateCameraClipPoints(transform.position,transform.rotation,ref collision.ajustedCameraClipPoints);
        collision.UpdateCameraClipPoints(destination,transform.rotation,ref collision.desiredCameraClipPoints);
        _direction = new Vector3(0, 0, 1);
        _verticalAngle = 0;
	}
	
	// Update is called once per frame
	void LateUpdate () {
        Vector3 newPosition = player.transform.position;
       
        Vector3 _finalDirection = Quaternion.AngleAxis(_verticalAngle,Vector3.Cross(_direction,Vector3.up) ) * _direction;

        adjustedDestination =player.transform.position - _finalDirection*adjustmentDistance;

        destination = player.transform.position - _finalDirection*cameraDistance;

      if (collision.colliding)
       {
            
            //adjustedDestination =GetInterpolatedPosition(newPosition);
            transform.position =  Vector3.SmoothDamp(transform.position,adjustedDestination,ref currVelocity,m_smoothTime);

        }
        else
      {
            
            //destination = GetInterpolatedPosition(newPosition);
          
            transform.position =  Vector3.SmoothDamp(transform.position,destination,ref currVelocity,m_smoothTime);
       }

       




        transform.LookAt(player.transform);

        collision.UpdateCameraClipPoints(transform.position,transform.rotation,ref collision.ajustedCameraClipPoints);
        collision.UpdateCameraClipPoints(destination,transform.rotation,ref collision.desiredCameraClipPoints);
        collision.CheckColliding(player.transform.position);
        adjustmentDistance = collision.GetAjustedDistanceWithRayFrom(player.transform.position);


        for (int i = 0; i < 5; i++)
        {
            Debug.DrawLine(player.transform.position, collision.desiredCameraClipPoints[i],Color.white);
            Debug.DrawLine(player.transform.position, collision.ajustedCameraClipPoints[i],Color.green);
        }

	}

    Vector3 GetInterpolatedPosition(Vector3 finalPosition)
    {
        
        finalPosition.y = finalPosition.y+35;
        float delta = m_smoothTime * Time.deltaTime;
        return new Vector3(Mathf.Lerp(transform.position.x,finalPosition.x,delta),Mathf.Lerp(transform.position.y,finalPosition.y+offset.y,delta),Mathf.Lerp(transform.position.z,finalPosition.z,delta)) ;
    }

    public class CollisionHandler
    {
        public LayerMask collisionLayer;

        [HideInInspector]
        public bool colliding = false;
        [HideInInspector]
        public Vector3[] ajustedCameraClipPoints;
        [HideInInspector]
        public Vector3[] desiredCameraClipPoints;

        Camera camera;

        public void Initialize(Camera cam)
        {
            camera = cam;
            ajustedCameraClipPoints = new Vector3[5];
            desiredCameraClipPoints = new Vector3[5];
            collisionLayer = (LayerMask)8;
        }

        public void UpdateCameraClipPoints(Vector3 cameraPosition, Quaternion atRotation, ref Vector3[] intoArray)
        {
            if (!camera)
                return;
            intoArray = new Vector3[5];

            float z = camera.nearClipPlane;
            float x= Mathf.Tan((camera.fieldOfView/3.41f))*z;
            float y = x / camera.aspect;

            intoArray[0] = (atRotation * new Vector3(-x, y, z)) + cameraPosition;
            intoArray[1] = (atRotation * new Vector3(x, y, z)) + cameraPosition;
            intoArray[2] = (atRotation * new Vector3(-x, -y, z)) + cameraPosition;
            intoArray[3] = (atRotation * new Vector3(x, -y, z)) + cameraPosition;
            intoArray[4] = cameraPosition - camera.transform.forward;
        }

        bool CollisionDetectedAtClipPoints(Vector3[] clipPonts, Vector3 fromPosition)
        {
            for (int i = 0; i < clipPonts.Length; i++)
            {
                Ray ray = new Ray(fromPosition, clipPonts[i] - fromPosition);;
                int mask1 =1<< 0;
                int mask2 =1<< 8;

                int mask =( (mask1 | mask2));
                float distance = Vector3.Distance(clipPonts[i],fromPosition);
                if (Physics.Raycast(ray, distance,mask))
                {
                    return true;
                }
            }
            return false;
        }



        public float GetAjustedDistanceWithRayFrom(Vector3 from)
        {
            float distance = -1;

            for (int i = 0; i < desiredCameraClipPoints.Length; i++)
            {
                Ray ray = new Ray(from,desiredCameraClipPoints[i]-from);
                RaycastHit hit;
                if(Physics.Raycast(ray,out hit))
                {
                    if (distance == -1)
                        distance = hit.distance;
                    else
                    {
                        if (hit.distance < distance)
                            distance = hit.distance;
                    }
                }
            }

            if (distance == -1)
                return 0;
            else
                return distance;
        }

        public void CheckColliding(Vector3 targetPosition)
        {
            if (CollisionDetectedAtClipPoints(desiredCameraClipPoints, targetPosition))
            {
                colliding = true;
            }
            else
            {
                colliding = false;
            }
        }
    }
}
