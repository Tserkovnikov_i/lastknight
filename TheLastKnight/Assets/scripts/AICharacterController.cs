﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace LastKnightCore
{
    enum AICharacterState
    {
        DailyRoutine,
        Agred
    }
    public class AICharacterController : BaseCharacterController {

        private NavMeshAgent _agent;
        private NavMeshPath _path;
        private GameObject _player;
        private FovRaycaster _fov;
        public List<Vector3> PatrolPoints;
        public float AttackTreshold = .7f;
        private int _currentPoint;
        private AICharacterState _state;


        protected override void Awake()
        {
            base.Awake();
            _player = GameObject.FindGameObjectsWithTag("Player")[0];
            _agent= GetComponent<NavMeshAgent>();
            _state = AICharacterState.DailyRoutine;
            _path = new NavMeshPath();
            _fov = GetComponent<FovRaycaster>();
   
            _currentPoint = 0;
      
         
        }
	// Update is called once per frame
	    void Update () {
            switch (_state)
            {
                case AICharacterState.DailyRoutine:
                    Patrol();
                    if (_fov.TargetDetected)
                        _state = AICharacterState.Agred;
                    break;
                case AICharacterState.Agred:
                    AgressiveUpdate();
                    break;
            }

	    }
        void Patrol()
        {
           
            if (PatrolPoints != null && PatrolPoints.Count != 0)
            {
                
                if (!_agent.hasPath)
                    _agent.SetDestination(PatrolPoints[_currentPoint]);
                if (_agent.remainingDistance<= _agent.stoppingDistance)
                {
                    if (_currentPoint + 1 < PatrolPoints.Count)
                        _currentPoint++;
                    else
                        _currentPoint = 0;
                    _agent.SetDestination(PatrolPoints[_currentPoint]);
                }
                _cInput.MoveInput  = 1;
    
               _cInput.Direction = (_agent.nextPosition-transform.position).normalized;
            
                _cInput.Direction.y = 0;
            }
        }
        void AgressiveUpdate()
        {
            _agent.SetDestination(_player.transform.position);


           
            if (_agent.remainingDistance > _agent.stoppingDistance)
            {
                
                _cInput.MoveInput  = 1;
         
                 _cInput.Direction = (_agent.nextPosition-transform.position).normalized;
       
            }
            else
            {
                
                _cInput.MoveInput  = 0;
                _cInput.Direction =( _player.transform.position-transform.position).normalized;
                _cInput.Direction.y = 0;
                if (Random.value < AttackTreshold)
                {
                    
                    if(_cInput.Hit.Activate())
                    Invoke("ResetAttack",3.0f);
                }
            }
        }

        void ResetAttack()
        {
            _cInput.Hit.Reset();
        }
    }
}
