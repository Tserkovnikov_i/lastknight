﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LastKnightCore
{
    public class Character: BaseGameObject  
    {
        [SerializeField] public float HP = 100;
        [SerializeField] public int STR = 10;
        [SerializeField] public int OneHandled = 10;
        [SerializeField] public float Defence = 10;
        [SerializeField] public float ArmorWearing = 10;
        [SerializeField]public CharacterMovement movement;
        Animator _animator;
        [SerializeField]BaseCharacterController controller;

        private Weapon _weapon;
        private bool _isAlive;
        protected override void Awake()
        {
            base.Awake();
            _animator = GetComponent<Animator>();
            _weapon = GetComponentInChildren<Weapon>();
            _isAlive = true;
            if(_weapon!=null)_weapon.onDamageCharacter += DamageOtherCharacter;
            movement.Init(_rigidbody,_animator,GetComponentInChildren<GroundCheckScript>(),GetComponent<Collider>(),transform.forward,_weapon);
        }

        void DamageOtherCharacter(Character other)
        {
            other.GetDamage(STR*(OneHandled/10)+_weapon.Damage);
        }
        void GetDamage(float damage)
        {
            HP -= damage - Defence * ArmorWearing / 10;
        }
        void Update()
        {
            if (HP > 0)
            {
                movement.CheckInput(controller.CInput);

                movement.ProcessMove();
                movement.ProcessAnimations();
            }
            else
            {
                if (_isAlive)
                {
                    _animator.SetTrigger("Dead");
                    _rigidbody.isKinematic = true;
                    foreach (Collider c in GetComponents<Collider>())
                    {
                        c.enabled = false;
                    }
                    if(controller!=null) controller.enabled = false;
                }

            }
        }

    }
}
