﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

namespace LastKnightCore
{
    
    [Serializable]
    public class AttackMovement:BaseMovement
    {
        public event Action OnComboHit;

        [SerializeField] float AttackTime = .2f;
        [SerializeField] Timing ActiveDamage;
        [SerializeField] Timing ComboTiming;
        private float _timeElapsed;
        private Weapon _weapon;
        private bool _comboHit;

        public AttackMovement()
        {
        }
        public override void Init(CharacterMovement cMovement)
        {
            base.Init(cMovement);
            _weapon = cMovement.CWeapon;
            ActiveDamage.onActivate+= delegate
            {
                    _weapon.IsInActivePhase = true;
            };
            ActiveDamage.onDeactivate+= delegate
            {
                    _weapon.IsInActivePhase = false;
            };
            _timeElapsed = 0f;
            _comboHit = false;
        }
        public override void CheckInput(CharacterInput input)
        {
            ComboTiming.Process(_timeElapsed);
            if (ComboTiming.IsActive)
            {
                if (input.Hit.GetIsActive()&&!_comboHit)
                {
                    _comboHit = true;
                    if(OnComboHit!=null) OnComboHit.Invoke();
                }
            }
        }
        public override void ProcessMove()
        {
            _front = 1;
            base.ProcessMove();
            _timeElapsed += Time.deltaTime;
            ActiveDamage.Process(_timeElapsed);
           



            if (_timeElapsed >= AttackTime)
            {
                
                _timeElapsed = 0;
                if (_comboHit)
                {
                    _comboHit = false;

                }
                else
                _characterMovement.State = CharacterMovementState.Grounded;
            }
        }

    }
}

