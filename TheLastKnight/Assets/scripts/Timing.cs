﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[Serializable]
public class Timing  {

    [SerializeField] public float StartTime= .2f;
    [SerializeField] public float EndTime = .45f;

    public event Action onActivate;
    public event Action onDeactivate;

    private bool _isActive;

    public bool IsActive
    {
        get
        {
            return _isActive;
        }
    }

    public void Process(float t)
    {
        if (t >= StartTime && t <= EndTime)
        {
            if(onActivate!=null)onActivate.Invoke();
            _isActive = true;
        }
        else
        {
            if(onDeactivate!=null)onDeactivate.Invoke();
            _isActive = false;
            
        }
    }
}
