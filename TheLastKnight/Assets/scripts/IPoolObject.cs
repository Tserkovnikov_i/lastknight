﻿using System;
using UnityEngine;

namespace LastKnightCore
{
   
    public interface IPoolObject
    {
       void Spawn(Vector3 position);
       bool IsActive{ get; set;}
        IPoolObject Instantiate();
    }
}

