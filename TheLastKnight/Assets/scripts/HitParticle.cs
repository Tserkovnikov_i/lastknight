﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LastKnightCore
{
    public class HitParticle : MonoBehaviour,IPoolObject {

        private ParticleSystem _particles;

        #region IPoolObject implementation
        public void Spawn(Vector3 position)
        {
            gameObject.transform.position = position;
            Debug.Log("Spawned" + position);
           
            _particles.Play();
            Invoke("ReturnToPool",_particles.main.duration);
        }
        public IPoolObject Instantiate()
        {
            return GameObject.Instantiate(this, Vector3.zero, Quaternion.identity);
        }
        public bool IsActive
        {
            get
            {
                return gameObject.activeInHierarchy;
            }
            set
            {
                gameObject.SetActive(value);
            }
        }
        #endregion

	// Use this for initialization
	void Awake () {
            _particles = GetComponent<ParticleSystem>();

	}
        void ReturnToPool()
        {
            IsActive = false;
        }
     
}
}