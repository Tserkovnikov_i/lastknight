﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LastKnightCore
{
    public class BaseCharacterController : MonoBehaviour {
        protected CharacterInput _cInput;
        public CharacterInput CInput
        {
            get
            {
                return _cInput; 
            }
            private set
            {
                _cInput = value;
            }
        }


        protected virtual void Awake()
        {
            
            _cInput = new CharacterInput();
        }

    }
}