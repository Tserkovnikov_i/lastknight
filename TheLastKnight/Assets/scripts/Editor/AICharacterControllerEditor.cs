﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace LastKnightCore
{
    
[CustomEditor( typeof(AICharacterController))]
public class AICharacterControllerEditor : Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
            GUILayout.Label("saa");
        }
        public void OnSceneGUI()
        {
            AICharacterController controller = target as AICharacterController;

            for (int i = 0; i < controller.PatrolPoints.Count; i++)
            {
                controller.PatrolPoints[i] = Handles.DoPositionHandle(controller.PatrolPoints[i], Quaternion.identity);
            }
        }
    }
}
