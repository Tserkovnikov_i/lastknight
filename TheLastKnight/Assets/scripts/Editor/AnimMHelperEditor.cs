﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace LastKnightCore
{

    [CustomEditor(typeof(AnimMHelper))]
    [CanEditMultipleObjects]
    public class AnimMHelperEditor : Editor
    {


        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
            AnimMHelper helper = target as AnimMHelper;
            if (GUILayout.Button("Attack"))
                helper.PlayAttack();
        }

    }
}